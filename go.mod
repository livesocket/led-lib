module gitlab.com/livesocket/led-lib

go 1.12

require (
	github.com/chebyrash/promise v0.0.0-20191019132353-81b8b936c6e4
	github.com/rpi-ws281x/rpi-ws281x-go v1.0.5
	gitlab.com/livesocket/conv v0.0.0-20191012101209-727e1c03a95c
	gitlab.com/livesocket/led-defs v0.0.0-20191024002145-976ef6e82026
)
