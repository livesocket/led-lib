package shapes

type Grid interface {
	Shape
	Rows() [][]uint32
	Row(index int) []uint32
	Set(x, y int, value uint32)
}
