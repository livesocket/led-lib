package shapes

import "gitlab.com/livesocket/led-lib/layer"

// SnakeGrid Represents a grid of leds
// constructed by "snaking" a single 1D
// LED strip back and forth in a grid
// pattern above each other
type SnakeGrid struct {
	Sections [][]uint32
	Origin   int
	Width    int
	Height   int
}

const (
	SnakeGridOriginBottomRight = 0
	SnakeGridOriginBottomLeft  = 1
	SnakeGridOriginTopRight    = 2
	SnakeGridOriginTopLeft     = 3
)

// NewSnakeGrid Creates a new "snake" grid
func NewSnakeGrid(rows, columns int, origin int) *SnakeGrid {
	sections := make([][]uint32, rows)
	for i := range sections {
		sections[i] = make([]uint32, columns)
	}
	return &SnakeGrid{
		Sections: sections,
		Origin:   origin,
		Width:    columns,
		Height:   rows,
	}
}

// Set Sets the value of the LED in the assigned
// place in the grid. 0,0 is ALWAYS assumed to be
// the bottom left corner of the grid
func (s *SnakeGrid) Set(x, y int, value uint32) {
	switch s.Origin {
	case SnakeGridOriginBottomRight:
		s.setForBottomRight(x, y, value)
	}
}

func (s *SnakeGrid) Rows() [][]uint32 {
	return s.Sections
}

func (s *SnakeGrid) Row(index int) []uint32 {
	return s.Sections[index]
}

func (s *SnakeGrid) GetLayer() *layer.Layer {
	result := layer.New()
	for r, section := range s.Sections {
		for c, item := range section {
			if item != 0 {
				result.Set(r*c, item)
			}
		}
	}
	return result
}

func (s *SnakeGrid) setForBottomRight(x, y int, value uint32) {
	if y%2 == 0 {
		x = s.Width - 1 - x
	}
	s.Sections[y][x] = value
}
