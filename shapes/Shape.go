package shapes

import "gitlab.com/livesocket/led-lib/layer"

type Shape interface {
	GetLayer() *layer.Layer
}
