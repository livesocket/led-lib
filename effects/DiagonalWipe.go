package effects

// import (
// 	"time"

// 	"gitlab.com/livesocket/led-lib/drivers"
// )

// // DiagonalWipe A diagonal wipe from corner to corner
// // Created by edorfaus(twitch)
// type DiagonalWipe struct {
// 	StripWidth int
// 	Speed      time.Duration
// 	Colors     []uint32
// }

// // NewDiagonalWipe Creates a new DiagonalWipe effect
// func NewDiagonalWipe(speed int, stripWidth int, colors ...uint32) *DiagonalWipe {
// 	return &DiagonalWipe{
// 		StripWidth: stripWidth,
// 		Speed:      time.Duration(speed),
// 		Colors:     colors,
// 	}
// }

// func (w *DiagonalWipe) GetPos(x int, y int) int {
// 	if y%2 == 0 {
// 		return y*w.StripWidth + w.StripWidth - 1 - x
// 	}
// 	return y*w.StripWidth + x
// }

// func (w *DiagonalWipe) DrawDiagonal(d drivers.Driver, start int, height int, color uint32) {
// 	y := start
// 	for x := 0; y >= 0 && x < w.StripWidth; x++ {
// 		if y < height {
// 			d.State()[w.GetPos(d, x, y)] = color
// 		}
// 		y--
// 	}
// }

// func (w *DiagonalWipe) Run(d drivers.Driver) error {
// 	// height = total count of LEDs in the strip / width
// 	height := len(d.State()) / w.StripWidth
// 	wipeLen := height
// 	if w.StripWidth > wipeLen {
// 		wipeLen = w.StripWidth
// 	}
// 	wipeLen += len(w.Colors)
// 	for pos := 0; pos < wipeLen; pos++ {
// 		for i, color := range w.Colors {
// 			w.DrawDiagonal(d, pos-i, height, color)
// 		}
// 		if err := d.Render(); err != nil {
// 			return err
// 		}
// 		time.Sleep(w.Speed)
// 	}
// 	return nil
// }
