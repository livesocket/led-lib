package effects

import (
	"time"

	"github.com/chebyrash/promise"
	"gitlab.com/livesocket/conv"
	"gitlab.com/livesocket/led-defs/defs"
	"gitlab.com/livesocket/led-lib/layer"
)

// Wipe Represents a wipe of light across an LED strip
type Wipe struct {
	Speed  time.Duration
	Colors []uint32
	Layer  *layer.Layer
	done   chan []uint32
}

// NewWipe Create a new wipe effect
func NewWipe(speed int, colors ...uint32) *Wipe {
	return &Wipe{
		Speed:  time.Duration(speed) * time.Microsecond,
		Colors: colors,
		Layer:  layer.New(),
		done:   make(chan []uint32),
	}
}

func WipeFromDef(definition *defs.Effect) (*Wipe, error) {
	speed, err := conv.ToInt(definition.Args["Speed"])
	if err != nil {
		return nil, err
	}
	colors, err := conv.ToUint32Slice(definition.Args["Colors"])
	if err != nil {
		return nil, err
	}
	return NewWipe(speed, colors...), nil
}

func (w *Wipe) GetLayer() *layer.Layer {
	return w.Layer
}

func (w *Wipe) Run(length int) *promise.Promise {
	return promise.New(func(resolve func(interface{}), reject func(error)) {
		for _, color := range w.Colors {
			for i := 0; i < length; i++ {
				w.Layer.Set(i, color)
				time.Sleep(w.Speed)
			}
		}
		resolve(nil)
	})
}

func (w *Wipe) Done() chan []uint32 {
	return w.done
}
