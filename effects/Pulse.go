package effects

import (
	"time"

	"github.com/chebyrash/promise"
	"gitlab.com/livesocket/led-lib/layer"
)

// Pulse Represents a pulse of light across an LED strip
type Pulse struct {
	Speed      time.Duration
	Size       int
	Color      uint32
	Position   int
	FromOrigin bool
	Layer      *layer.Layer
}

// NewPulse Create a new pulse effect
func NewPulse(speed int, size int, color uint32, fromOrigin bool) *Pulse {
	return &Pulse{
		Speed:      time.Duration(speed) * time.Microsecond,
		Size:       size,
		Color:      color,
		Position:   0 - size,
		FromOrigin: fromOrigin,
		Layer:      layer.New(),
	}
}

func (p *Pulse) GetLayer() *layer.Layer {
	return p.Layer
}

func (p *Pulse) Run(length int) *promise.Promise {
	return promise.New(func(resolve func(interface{}), reject func(error)) {
		for i := 0 - p.Size; i < length; i++ {
			p.Layer.Clear()
			for s := 0; s < p.Size; s++ {
				p.Layer.Set(i+s, p.Color)
			}
			time.Sleep(p.Speed)
		}
		resolve(nil)
	})
}
