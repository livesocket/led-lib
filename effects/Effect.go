package effects

import (
	"errors"

	"github.com/chebyrash/promise"
	"gitlab.com/livesocket/led-defs/defs"
	"gitlab.com/livesocket/led-lib/layer"
)

type Effect interface {
	GetLayer() *layer.Layer
	Run(length int) *promise.Promise
}

func CreateFromDef(definition *defs.Effect) (Effect, error) {
	switch definition.Name {
	case "Wipe":
		return WipeFromDef(definition)
	default:
		return nil, errors.New("unsupported effect definition")
	}
}
