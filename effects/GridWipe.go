package effects

import (
	"time"

	"github.com/chebyrash/promise"
	"gitlab.com/livesocket/led-lib/layer"
	"gitlab.com/livesocket/led-lib/shapes"
)

type GridWipe struct {
	Speed    time.Duration
	Color    uint32
	Position int
	Grid     shapes.Grid
	Layer    *layer.Layer
}

func NewGridWipe(grid shapes.Grid, speed int, color uint32) *GridWipe {
	return &GridWipe{
		Speed:    time.Duration(speed) * time.Microsecond,
		Color:    color,
		Position: -1,
		Grid:     grid,
		Layer:    layer.New(),
	}
}

func (w *GridWipe) GetLayer() *layer.Layer {
	return w.Layer
}

func (w *GridWipe) Run(length int) *promise.Promise {
	return promise.New(func(resolve func(interface{}), reject func(error)) {
		for x := 0; x < len(w.Grid.Rows()[0]); x++ {
			for y := range w.Grid.Rows() {
				println("Set", x, y, w.Color)
				w.Grid.Set(x, y, w.Color)
			}
			w.Layer = w.Grid.GetLayer()
			time.Sleep(w.Speed)
		}
		resolve(nil)
	})
}
