## Build for Raspberry PI

GOOS=linux GOARCH=arm GOARM=5 go build

## Config

Create a `livesocket.json` file beside the binary

```json
{
  "Address": "wss://router.url/ws/",
  "Realm": "chatbot",
  "User": "The Backend User",
  "Pword": "The Backend Password"
}
```

Replace placeholders with values
