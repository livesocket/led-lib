package animations

type Frame struct {
	Leds     []uint32 `json:"leds"`
	Duration uint64   `json:"duration"`
}
