package animations

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type Animation struct {
	Name   string   `json:"name"`
	Frames []*Frame `json:"frames"`
	Loop   uint64   `json:"loop"`
}

func LoadAnimationFromFile(path string) (*Animation, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	bytes, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	var animation Animation
	err = json.Unmarshal(bytes, &animation)
	if err != nil {
		panic(err)
		return nil, err
	}

	return &animation, nil
}
