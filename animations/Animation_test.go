package animations_test

import (
	"testing"

	"gitlab.com/livesocket/led-lib/animations"
)

func TestLoadAnimationFromFile(t *testing.T) {

	animation, err := animations.LoadAnimationFromFile("animation.json")
	if err != nil {
		t.Error(err)
	}

	if animation.Name != "clean" {
		t.Error("incorrect name")
	}
}
