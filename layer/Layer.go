package layer

import "sync"

type Layer struct {
	sync.RWMutex
	internal map[int]uint32
}

func New() *Layer {
	return &Layer{
		internal: make(map[int]uint32),
	}
}

func (l *Layer) Keys() []int {
	l.RLock()
	keys := make([]int, len(l.internal))
	i := 0
	for k := range l.internal {
		keys[i] = k
		i++
	}
	l.RUnlock()
	return keys
}

func (l *Layer) Get(key int) (value uint32, ok bool) {
	l.RLock()
	result, ok := l.internal[key]
	l.RUnlock()
	return result, ok
}

func (l *Layer) Set(key int, value uint32) {
	l.Lock()
	l.internal[key] = value
	l.Unlock()
}

func (l *Layer) Clear() {
	l.internal = make(map[int]uint32)
}
