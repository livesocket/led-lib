package drivers

import (
	ws2811 "github.com/rpi-ws281x/rpi-ws281x-go"
	"gitlab.com/livesocket/led-lib/effects"
)

// LEDStrip Represents a LED strip
type LEDStrip struct {
	WS *ws2811.WS2811
	*DriverBase
}

// NewLEDStrip Creates a new LED strip
func NewLEDStrip(count int, brightness int) (*LEDStrip, error) {
	opt := ws2811.DefaultOptions
	opt.Channels[0].Brightness = brightness
	opt.Channels[0].LedCount = count

	ws, err := ws2811.MakeWS2811(&opt)
	if err != nil {
		return nil, err
	}

	return &LEDStrip{
		WS: ws,
		DriverBase: &DriverBase{
			Count:      count,
			Brightness: brightness,
			Effects:    []effects.Effect{},
			Base:       make([]uint32, count),
		},
	}, nil
}

// Init Init ws281x controller
func (s *LEDStrip) Init() {
	s.WS.Init()
}

// Fini Deinit ws281x controller
func (s *LEDStrip) Fini() {
	s.WS.Fini()
}

// Render Render the strip
func (s *LEDStrip) Render() error {
	// overlay := s.FlattenLayers()
	for i := range s.Base {
		// color := s.DriverBase.Base[i]
		// if value, _ := overlay.Get(i); value != 0 {
		// 	color = value
		// }
		s.WS.Leds(0)[i] = s.Base[i]
	}
	return s.WS.Render()
}
