package drivers

import (
	"time"

	"gitlab.com/livesocket/led-lib/animations"
	"gitlab.com/livesocket/led-lib/effects"
)

// Driver Represents a renderable strip
type Driver interface {
	Init()
	Fini()
	SetBase(data []uint32)
	SetAll(color uint32)
	RunAnimation(animation *animations.Animation)
	RunFrame(frame *animations.Frame)
	// Run(effect effects.Effect)
	// RunAsync(effect effects.Effect) *promise.Promise
	Length() int
	Render() error
}

// DriverBase Base driver logic
type DriverBase struct {
	Count      int
	Brightness int
	Effects    []effects.Effect
	Base       []uint32
}

func (d *DriverBase) SetBase(data []uint32) {
	d.Base = data
}

func (d *DriverBase) SetAll(color uint32) {
	for i := 0; i < d.Length(); i++ {
		d.Base[i] = color
	}
}

func (d *DriverBase) RunAnimation(animation *animations.Animation) {
	loop := animation.Loop
	for loop != 0 {
		for _, frame := range animation.Frames {
			d.RunFrame(frame)
		}
		loop--
	}
}

func (d *DriverBase) RunFrame(frame *animations.Frame) {
	d.SetBase(frame.Leds)
	time.Sleep(time.Duration(frame.Duration) * time.Millisecond)
}

// func (d *DriverBase) Run(effect effects.Effect) {
// 	d.Effects = append(d.Effects, effect)
// 	fmt.Printf("%v", d.Effects)
// 	effect.Run(d.Count).Await()
// 	fmt.Printf("Effect complete")
// 	d.removeEffect(effect)
// 	fmt.Printf("%v", d.Effects)
// }

// func (d *DriverBase) RunAsync(effect effects.Effect) *promise.Promise {
// 	return promise.New(func(resolve func(interface{}), reject func(error)) {
// 		d.Run(effect)
// 		resolve(nil)
// 	})
// }

func (d *DriverBase) Length() int {
	return d.Count
}

// func (d *DriverBase) removeEffect(effect effects.Effect) {
// 	for i, e := range d.Effects {
// 		if effect == e {
// 			copy(d.Effects[i:], d.Effects[i+1:])
// 			d.Effects[len(d.Effects)-1] = nil
// 			d.Effects = d.Effects[:len(d.Effects)-1]
// 		}
// 	}
// }

// func (d *DriverBase) FlattenLayers() *layer.Layer {
// 	result := layer.New()
// 	for _, e := range d.Effects {
// 		l := e.GetLayer()
// 		for _, key := range l.Keys() {
// 			if v, _ := result.Get(key); v == 0 {
// 				value, _ := l.Get(key)
// 				result.Set(key, value)
// 			}
// 		}
// 	}
// 	return result
// }
