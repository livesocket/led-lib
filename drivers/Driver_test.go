package drivers_test

import (
	"testing"

	"gitlab.com/livesocket/led-lib/animations"
	"gitlab.com/livesocket/led-lib/drivers"
)

func TestDriverSetBase(t *testing.T) {
	driver := &drivers.DriverBase{}

	data := []uint32{1, 2, 3, 4, 5}

	driver.SetBase(data)

	if driver.Base[0] != 1 {
		t.Error("incorrect value in base")
	}

	if driver.Base[1] != 2 {
		t.Error("incorrect value in base")
	}

	if driver.Base[2] != 3 {
		t.Error("incorrect value in base")
	}
}

func TestDriverRunFrame(t *testing.T) {
	driver := &drivers.DriverBase{}

	frame := &animations.Frame{Leds: []uint32{1, 2, 3, 4, 5}, Duration: 1}

	driver.RunFrame(frame)

	if driver.Base[0] != 1 {
		t.Error("incorrect value in base")
	}

	if driver.Base[1] != 2 {
		t.Error("incorrect value in base")
	}

	if driver.Base[2] != 3 {
		t.Error("incorrect value in base")
	}
}
